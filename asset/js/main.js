// souris

const souris = document.querySelector('.cursor');

souris.style.display = 'none';
document.addEventListener('mousemove', function (e){
    let x = e.clientX;
    let y = e.clientY;

    souris.style.top = y + "px";
    souris.style.left = x + "px";
    souris.style.display = 'block';
});

document.addEventListener('mouseout', function (){
    souris.style.display = 'none';
});


// header fixed en scroll

const header = document.querySelector('#masthead');

document.addEventListener('scroll', function (){
    header.style.position = 'fixed'
})